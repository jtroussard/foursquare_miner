#!/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 17 20:19:20 2016

@author: Jacques Troussard
@version: 1.0.0
"""

from urllib.request import urlopen
from bs4 import BeautifulSoup as bs
from pprint import pprint
import re

url = "https://developer.foursquare.com/docs/venues/search"

html_content = urlopen(url).read()

soup = bs(html_content, 'html.parser')
table_soup = soup.find_all('table')
table_guts = table_soup[1]

clean_table = []
loader = []

# The cleaning of this HTML file is very hookey as it stands. First try with 
# BeautifulSoup, but going through a normal, table wasn't so bad were I was
# able to separate the TH and TD tags, it was the introduction of a table
# within a table that caused some problems. As of now the list produced by
# this cleaning is fairly clean but two major issues remain
# 1) the algorithum isn't very strong, espcially for the inner table. I suspect
#    that the slightest change on foursquares part will break this code.)
# 2) Tables within tables is still loaded into the result list as if they were
#    regular strings part of the main table. When putting this information back
#    together for the main data mining program there will obviously be some
#    hands on to make it work. Ultimately I was hoping to have this process
#    completely automated. For the time being program will have some organic
#    qualities to it, that will require some monitoring, until I learn a better
#    way to implement this. 
counter = 0
for goop in table_guts('tr',):
    for stuff in goop:
        curr_stuff = str(stuff)[0:10]
        if (str(type(stuff)) != '<class \'bs4.element.NavigableString\'>'):
            if(stuff.name == 'th' and not loader):
                counter += 1
                loader.append(stuff.string)
                flag = True
            elif(stuff.name == 'th' and loader):
                counter += 1
                clean_table.append(loader)
                loader = []
                loader.append(stuff.string)
            elif(stuff.name == 'td'):
                if(not stuff.table):
                    if(stuff.get_text() == 'NONE'):
                        print('check')
                    loader.append(stuff.get_text())
if (counter > len(clean_table)):
    clean_table.append(loader)

    
pprint(clean_table)
    