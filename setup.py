#!/bin/env python3

try:
from setuptools import setup
except ImportError:
from distutils.core import setup

config = {
	'description': 'My Project',
	'author': 'Jacques Troussard',
	'url': 'http://cs.umw.edu/~jtroussa/public_html/',
	'download_url': 'http://cs.umw.edu/~jtroussa/public_html/',
	'author_email': 'jtroussa@mail.umw.edu',
	'version': '0.1',
	'install_requires': ['nose'],
	'packages': ['NAME'],
	'scripts': [],
	'name': 'projectname'
}

setup(**config)
